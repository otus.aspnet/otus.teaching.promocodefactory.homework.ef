﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess.EF;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    /// <summary>
    /// Расширение для добавления dbcontext
    /// </summary>
    public static class DataAccessExtention
    {
        public static IServiceCollection AddDb(this IServiceCollection services, string connectionstring)
        {
            return services.AddDbContext<ApplicationContext>(options => {
                options.UseSqlite(connectionstring);
                options.UseLazyLoadingProxies();
            });
        }
    }
}
