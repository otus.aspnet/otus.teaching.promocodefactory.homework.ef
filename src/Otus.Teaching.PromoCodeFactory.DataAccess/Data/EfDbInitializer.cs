﻿using Otus.Teaching.PromoCodeFactory.DataAccess.EF;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly ApplicationContext _dataContext;

        public EfDbInitializer(ApplicationContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async void InitializeDb()
        {
            //await _dataContext.Database.EnsureDeletedAsync();
            //await _dataContext.Database.EnsureCreatedAsync();

            //await _dataContext.AddRangeAsync(FakeDataFactory.Employees);
            //await _dataContext.SaveChangesAsync();

            //await _dataContext.AddRangeAsync(FakeDataFactory.Preferences);
            //await _dataContext.SaveChangesAsync();

            //foreach (var item in FakeDataFactory.Customers)
            //{
            //    var customer = new Customer(item);
            //    var preferences = _dataContext.Preferences.Where(x => item.Preferences.Select(p => p.Name).Contains(x.Name)).ToList();
            //    customer.Preferences = preferences;
            //    await _dataContext.Customers.AddAsync(customer);
            //}

            //await _dataContext.SaveChangesAsync();

        }
    }
}
