﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.EF
{
    public class ApplicationContext : DbContext
    {
        public DbSet<PromoCode> PromoCodes { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Employee> Employees { get; set; }


        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Preferences)
                .WithMany(p => p.Customers)
                .UsingEntity<CustomerPreference>(
                    j => j
                        .HasOne(cp => cp.Preference)
                        .WithMany(p => p.CustomerPreferences)
                        .HasForeignKey(cp => cp.PreferenceId),
                    j => j
                        .HasOne(cp => cp.Customer)
                        .WithMany(c => c.CustomerPreferences)
                        .HasForeignKey(cp => cp.CustomerId),
                    j =>
                    {
                        j.HasKey(cp => new { cp.CustomerId, cp.PreferenceId });
                    });

            modelBuilder.Entity<Customer>()
                .HasIndex(c => c.Email).IsUnique();

            //modelBuilder.Entity<PromoCode>()
            //    .HasOne(p => p.Preference)
            //    .WithMany(pc => pc.PromoCodes);
        }

    }
}
