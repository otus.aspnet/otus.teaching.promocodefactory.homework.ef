﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public interface IDbInitializer
    {
        public void InitializeDb();
    }
}
