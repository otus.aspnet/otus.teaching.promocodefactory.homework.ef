﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        private readonly ApplicationContext _db;
        private readonly DbSet<T> _dbSet;

        public EfRepository(ApplicationContext dataContext)
        {
            _db = dataContext;
            _dbSet = dataContext.Set<T>();
        }

        /// <inheritdoc />
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _dbSet.ToListAsync(); ;
        }

        /// <inheritdoc />
        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _dbSet.FindAsync(id);
        }

        /// <inheritdoc />
        public async Task<IEnumerable<T>> GetByIdsAsync(List<Guid> ids)
        {
            _ = ids ?? throw new NullReferenceException();
            var items = await _dbSet.Where(x => ids.Contains(x.Id)).ToListAsync();
            return items;
        }

        /// <inheritdoc />
        public async Task<IEnumerable<T>> GetOnConditionAsync(Expression<Func<T, bool>> predicate)
        {
            return await _dbSet.Where(predicate).ToListAsync();
        }

        /// <inheritdoc />
        public async Task AddAsync(T item)
        {
            _ = item ?? throw new NullReferenceException();
            await _dbSet.AddAsync(item);
            await _db.SaveChangesAsync();
        }

        /// <inheritdoc />
        public async Task UpdateAsync()
        {
            await _db.SaveChangesAsync();
        }

        public async Task DeleteAsync(T item)
        {
            _dbSet.Remove(item);
            await _db.SaveChangesAsync();
        }
    }
}
