﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customersRepository;
        public PromocodesController(
            IRepository<PromoCode> promoCodeRepository,
            IRepository<Preference> preferenceRepository,
            IRepository<Customer> customersRepository)
        {
            
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _customersRepository = customersRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();
            var promoCodesShort = promoCodes.Select(x => new PromoCodeShortResponse(x)).ToList();
            return Ok(promoCodesShort);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preferences = await _preferenceRepository.GetOnConditionAsync(x => x.Name == request.Preference);
            if (preferences == null)
                return NotFound();

            var preference = preferences.First();
            var customers = await _customersRepository.GetOnConditionAsync(x => x.Preferences.Any(p => p.Id == preference.Id));

            foreach (var customer in customers)
            {
                customer.Promocodes.Add(new PromoCode()
                {
                    PartnerName = request.PartnerName,
                    Code = request.PromoCode,
                    ServiceInfo = request.ServiceInfo,

                    BeginDate = DateTime.Now,
                    EndDate = DateTime.Now.AddDays(30),

                    Preference = preference
                });
            }
            await _customersRepository.UpdateAsync();
            return Ok();

        }
    }
}