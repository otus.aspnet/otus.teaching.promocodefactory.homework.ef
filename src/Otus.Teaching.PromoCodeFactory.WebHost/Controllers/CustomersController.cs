﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NSwag.Annotations;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{

    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<Preference> preferenceRepository)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;

        }
        /// <summary>
        /// Возвращает список клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SwaggerResponse(HttpStatusCode.OK, typeof(CustomerShortResponse))]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();

            var customersModelList = customers.Select(x =>
                new CustomerShortResponse(x)).ToList();

            return Ok(customersModelList);
        }

        /// <summary>
        /// Возвращает данные клиента по id
        /// </summary>
        /// <param name="id">id клиента</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        [SwaggerResponse(HttpStatusCode.OK,typeof(CustomerResponse))]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            var customerResponse = new CustomerResponse(customer);
            return Ok(customerResponse);
        }

        /// <summary>
        /// Добавление нового клиента.
        /// </summary>
        /// <param name="request">Данные клиента</param>
        /// <returns></returns>
        [HttpPost]
        [SwaggerResponse(HttpStatusCode.Created, typeof(Guid))]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences = await _preferenceRepository.GetByIdsAsync(request.PreferenceIds);
            var customer = new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.LastName,
                Preferences = preferences.ToList(),
            };

            await _customerRepository.AddAsync(customer);

            return CreatedAtAction("CreateCustomer", customer.Id);
        }

        /// <summary>
        /// Обновление информации о клиенте.
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <param name="request">Обновлённые данные клиента</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [SwaggerResponse(HttpStatusCode.OK, typeof(Guid))]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer is null)
                return NotFound();

            var preferences = await _preferenceRepository.GetByIdsAsync(request.PreferenceIds);

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.LastName;
            customer.Preferences.Clear();
            customer.Preferences = preferences.ToList();

            await _customerRepository.UpdateAsync();

            return Ok(customer.Id);
        }

        /// <summary>
        /// Удаление клиента.
        /// </summary>
        /// <param name="id">Id клиента</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer is null)
                return NotFound();

            await _customerRepository.DeleteAsync(customer);

            return Ok();
        }
    }
}