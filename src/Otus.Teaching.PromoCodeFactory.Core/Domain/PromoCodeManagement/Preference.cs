﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        [MaxLength(50)]
        public string Name { get; set; }

        public virtual List<Customer> Customers { get; set; }

        public virtual List<CustomerPreference> CustomerPreferences { get; set; } = new List<CustomerPreference>();

        public virtual List<PromoCode> PromoCodes { get; set; } = new List<PromoCode>();

    }
}