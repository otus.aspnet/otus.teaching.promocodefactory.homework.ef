﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(25)]
        public string FirstName { get; set; }
        [MaxLength(50)]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(50)]
        public string Email { get; set; }

        public virtual List<Preference> Preferences { get; set; } = new List<Preference>();
        public virtual List<CustomerPreference> CustomerPreferences { get; set; } = new List<CustomerPreference>();


        public virtual List<PromoCode> Promocodes { get; set; } = new List<PromoCode>();

        public Customer()
        {
                
        }
        public Customer(Customer customer)
        {
            FirstName = customer.FirstName;
            LastName = customer.LastName;
            Email = customer.Email;
        }
    }
}